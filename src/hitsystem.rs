use nalgebra::Vector3;

use crate::math::*;
use crate::objects::{Material, Object, Shape};
use crate::ray::Ray;

pub struct HitRecord {
    pub p: Point,
    pub normal: Vector3<f64>,
    pub t: f64,
    pub front_face: bool,
    pub material: Material,
}

impl HitRecord {
    fn new(r: &Ray, center: &Point, radius: f64, temp: f64, material: Material) -> HitRecord {
        let t = temp;
        let p = r.get_at(t);
        let outward_normal = (p - center) / radius;
        let front_face = dot(&r.get_direction(), &outward_normal) < 0.0;
        let normal = if front_face {
            outward_normal.clone()
        } else {
            -outward_normal.clone()
        };

        HitRecord {
            p,
            normal,
            t,
            front_face,
            material,
        }
    }
}

pub fn hit(object: &Object, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
    match object.get_shape() {
        Shape::Sphere(center, radius) => hit_sphere(
            &center,
            radius.clone(),
            r,
            t_min,
            t_max,
            object.get_material(),
        ),
    }
}

fn hit_sphere(
    center: &Point,
    radius: f64,
    r: &Ray,
    t_min: f64,
    t_max: f64,
    material: Material,
) -> Option<HitRecord> {
    let oc = r.get_origin() - center;
    let a = r.get_direction().magnitude_squared();
    let half_b = dot(&oc, &r.get_direction());
    let c = oc.magnitude_squared() - radius * radius;
    let discriminant = half_b * half_b - a * c;

    if discriminant > 0.0 {
        let root = discriminant.sqrt();
        let temp = (-half_b - root) / a;
        if temp > t_min && temp < t_max {
            return Some(HitRecord::new(r, center, radius, temp, material));
        }

        let temp = (-half_b + root) / a;
        if temp > t_min && temp < t_max {
            return Some(HitRecord::new(r, center, radius, temp, material));
        }
    }

    None
}

pub struct MaterialResult {
    attenuation: Color,
    scattered: Ray,
}

impl MaterialResult {
    fn new(attenuation: Color, scattered: Ray) -> MaterialResult {
        MaterialResult {
            attenuation,
            scattered,
        }
    }

    pub fn get_attenuation(&self) -> Color {
        self.attenuation
    }

    pub fn get_scattered(&self) -> Ray {
        self.scattered
    }
}

pub fn scatter(r_in: &Ray, rec: &HitRecord) -> Option<MaterialResult> {
    match rec.material {
        Material::Lambertian(albedo) => {
            let scatter_direction = rec.normal + random_unit_vector();
            let scattered = Ray::new(rec.p, scatter_direction);
            let attenuation = albedo;
            Some(MaterialResult::new(attenuation, scattered))
        }
        Material::Metal(albedo, f) => {
            let fuzz = if f < 1.0 { f } else { 1.0 };
            let reflected = reflect(&r_in.get_direction().normalize(), &rec.normal);
            let scattered = Ray::new(rec.p, reflected + fuzz * random_in_unit_sphere());
            let attenuation = albedo;
            Some(MaterialResult::new(attenuation, scattered))
        }
    }
}

fn reflect(v: &Vector3<f64>, n: &Vector3<f64>) -> Vector3<f64> {
    v - 2.0 * dot(v, n) * n
}

#[derive(Debug, Clone)]
pub struct HitSystem {
    objects: Vec<Object>,
}

impl HitSystem {
    pub fn new() -> HitSystem {
        HitSystem { objects: vec![] }
    }

    pub fn add(&mut self, object: Object) {
        self.objects.push(object);
    }

    pub fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut temp_rec: Option<HitRecord> = None;
        let mut closest_so_far = t_max;

        for object in self.objects.iter() {
            if let Some(data) = hit(&object, r, t_min, closest_so_far) {
                closest_so_far = data.t;
                temp_rec = Some(data);
            }
        }

        temp_rec
    }
}
