use nalgebra::Vector3;

use crate::math::*;

#[derive(Debug, Clone, Copy)]
pub struct Ray {
    origin: Point,
    direction: Vector3<f64>,
}

impl Ray {
    pub fn new(origin: Point, direction: Vector3<f64>) -> Self {
        Ray { origin, direction }
    }

    pub fn get_origin(&self) -> Point {
        self.origin
    }

    pub fn get_direction(&self) -> Vector3<f64> {
        self.direction
    }

    pub fn get_at(&self, t: f64) -> Point {
        self.origin + t * self.direction
    }
}
