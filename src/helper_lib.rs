use image::math::utils;
use nalgebra::Vector3;

use crate::hitsystem::{scatter, HitSystem};
use crate::math::*;
use crate::ray::Ray;

pub fn vec_row_mult(vec1: &Vector3<f64>, vec2: &Vector3<f64>) -> Vector3<f64> {
    Vector3::new(vec1[0] * vec2[0], vec1[1] * vec2[1], vec1[2] * vec2[2])
}

pub fn ray_color(r: &Ray, world: &HitSystem, depth: u32) -> Color {
    let mut cur_ray = *r;
    let mut cur_attenuation = color(1.0, 1.0, 1.0);
    for _ in 0..depth {
        if let Some(data) = world.hit(&cur_ray, 0.001, INFINITY) {
            if let Some(mat) = scatter(&cur_ray, &data) {
                cur_attenuation = vec_row_mult(&cur_attenuation, &mat.get_attenuation());
                cur_ray = mat.get_scattered();
            }
        } else {
            let unit_direction = cur_ray.get_direction().normalize();
            let t = 0.5 * (unit_direction.y + 1.0);
            let c = (1.0 - t) * color(1.0, 1.0, 1.0) + t * color(0.5, 0.7, 1.0);
            return vec_row_mult(&cur_attenuation, &c);
        }
    }
    color(0.0, 0.0, 0.0)
}

pub fn write_color(pixel_color: &Color, samples_per_pixel: f64) -> [u8; 3] {
    let mut r = pixel_color.x;
    let mut g = pixel_color.y;
    let mut b = pixel_color.z;

    let scale = 1.0 / samples_per_pixel;
    r = (scale * r).sqrt();
    g = (scale * g).sqrt();
    b = (scale * b).sqrt();
    let r = (256.0 * utils::clamp(r, 0.0, 0.999)) as u8;
    let g = (256.0 * utils::clamp(g, 0.0, 0.999)) as u8;
    let b = (256.0 * utils::clamp(b, 0.0, 0.999)) as u8;
    [r, g, b]
}
