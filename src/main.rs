use std::sync::{Arc, Mutex};

use image::RgbImage;

mod camera;
mod helper_lib;
mod hitsystem;
mod math;
mod objects;
mod ray;
mod threads;

use camera::Camera;
use hitsystem::HitSystem;
use math::*;
use objects::{Material, Object, Shape};
use threads::multi_threading;

const MAX_DEPTH: u32 = 50;
const ASPECT_RATIO: f64 = 16.0 / 9.0;
const IMAGE_WIDTH: u32 = 1080; // 384
const IMAGE_HEIGHT: u32 = (IMAGE_WIDTH as f64 / ASPECT_RATIO) as u32;
const SAMPLES_PER_PIXEL: u32 = 100;
const THREADCOUNT: u32 = 6;

fn main() {
    let image_buffer = RgbImage::new(IMAGE_WIDTH, IMAGE_HEIGHT);

    let mut world = HitSystem::new();

    let material_ground = Material::Lambertian(color(0.8, 0.8, 0.0));
    let material_center = Material::Lambertian(color(0.7, 0.3, 0.3));
    let material_left = Material::Metal(color(0.8, 0.8, 0.8), 0.3);
    let material_right = Material::Metal(color(0.8, 0.6, 0.2), 1.0);

    world.add(Object::new(
        Shape::Sphere(point(0.0, -100.5, -1.0), 100.0),
        material_ground,
    ));
    world.add(Object::new(
        Shape::Sphere(point(0.0, 0.0, -1.0), 0.5),
        material_center,
    ));
    world.add(Object::new(
        Shape::Sphere(point(-1.0, 0.0, -1.0), 0.5),
        material_left,
    ));
    world.add(Object::new(
        Shape::Sphere(point(1.0, 0.0, -1.0), 0.5),
        material_right,
    ));

    let cam = Camera::new();
    multi_threading(
        Arc::new(cam),
        Arc::new(world),
        Arc::new(Mutex::new(image_buffer)),
    );
}
