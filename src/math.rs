use nalgebra::Vector3;
use rand::distributions::Uniform;
use rand::prelude::*;

pub type Color = Vector3<f64>;
pub type Point = Vector3<f64>;

pub const INFINITY: f64 = f64::INFINITY;
pub const PI: f64 = std::f64::consts::PI;

pub fn vec3(x: f64, y: f64, z: f64) -> Vector3<f64> {
    Vector3::new(x, y, z)
}

pub fn color(r: f64, g: f64, b: f64) -> Color {
    Color::new(r, g, b)
}

pub fn point(x: f64, y: f64, z: f64) -> Point {
    Point::new(x, y, z)
}

pub fn dot(v1: &Vector3<f64>, v2: &Vector3<f64>) -> f64 {
    v1.dot(v2)
}

pub fn random_in_unit_sphere() -> Vector3<f64> {
    loop {
        let p: Vector3<f64> =
            Vector3::from_distribution(&Uniform::new_inclusive(-1.0, 1.0), &mut thread_rng());
        if !(p.magnitude_squared() >= 1.0) {
            return p;
        }
    }
}

pub fn random_unit_vector() -> Vector3<f64> {
    let mut rng = thread_rng();
    let a: f64 = rng.gen_range(0.0, 2.0 * PI);
    let z: f64 = rng.gen_range(-1.0, 1.0);
    let r: f64 = (1.0 - z * z).sqrt();
    vec3(r * a.cos(), r * a.sin(), z)
}

pub fn random_in_hemisphere(normal: &Vector3<f64>) -> Vector3<f64> {
    let in_unit_sphere = random_in_unit_sphere();
    if dot(&in_unit_sphere, normal) > 0.0 {
        in_unit_sphere
    } else {
        -in_unit_sphere
    }
}
