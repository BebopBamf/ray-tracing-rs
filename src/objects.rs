use crate::math::*;

#[derive(Debug, Clone, Copy)]
pub struct Object {
    shape: Shape,
    material: Material,
}

impl Object {
    pub fn new(shape: Shape, material: Material) -> Object {
        Object { shape, material }
    }

    pub fn get_shape(&self) -> Shape {
        self.shape
    }

    pub fn get_material(&self) -> Material {
        self.material
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Shape {
    Sphere(Point, f64),
}

#[derive(Debug, Clone, Copy)]
pub enum Material {
    Lambertian(Color),
    Metal(Color, f64),
}
